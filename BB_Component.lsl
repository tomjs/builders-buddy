///////////////////////////////////////////////////////////////////////////////
// Builders' Buddy 1.10 (Component Script)
// by Newfie Pendragon, 2006-2008
///////////////////////////////////////////////////////////////////////////////
//
// 1.10-lt2     Add log(), warning(), debug() functions and some debugging output
// 1.10-lt3     Move llSetRot() ahead of moving in do_move()
// 1.10-lt6     Add fixes for OpenSim (from https://www.outworldz.com/cgi/freescripts.plx?ID=1119)
//              * Added a Memory datastorage for OpenSim using osMakeNotecard
// 1.10-lt7     Add memory limits
//
// Script Purpose & Use
// Functions are dependent on the "component script"
//
// QUICK USE:
// - Set the default channel in both scripts
// - Drop the "Base" script in the Base object
// - Drop the "Component" Script in each build object
// - Touch your Base, and choose RECORD
// - Take all building parts into inventory
// - Drag building parts from inventory into Base Prim
// - Touch your base and choose BUILD
//
// OTHER COMMANDS from the Touch menu
// - To reposition, move/rotate Base Prim choose POSITION
// - To lock into position (removes scripts) choose DONE
// - To delete building pieces: choose CLEAN
///////////////////////////////////////////////////////////////////////////////
// This script is copyrighted material, and has a few (minor) restrictions.
// For complete details, including a revision history, please see
// http://wiki.secondlife.com/wiki/Builders_Buddy
///////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////
// Configurable Settings
float fTimerInterval = 0.25;        // Time in seconds between movement 'ticks'
integer DefaultChannel = -192567;   // Andromeda Quonset's default channel
integer PRIMCHAN = -192567;         // Channel used by Base Prim to talk to Component Prims;
                                    // ***THIS MUST MATCH IN BOTH SCRIPTS!***

// Set to FALSE if you dont want the script to say anything while 'working'
// Set to >1 to increase verbosity
// 0 = quiet    (FALSE)
// 1 = minimal  (TRUE)
// 2 = warning
// 3 = debug
integer chatty = 2;

// Memory limit
integer MEM_LIMIT = 48000;

//////////////////////////////////////////////////////////////////////////////////////////
// Runtime Variables (Dont need to change below here unless making a derivative)
vector vOffset;
rotation rRotation;
integer bNeedMove;
vector vDestPos;
rotation rDestRot;
integer bMovingSingle = FALSE;
integer bAbsolute = FALSE;
integer bRecorded = FALSE;

// lt-6: Add notecard support
vector Region_Size;
vector vLimit;

list record_mem = [];   // Memory to be stored
key g_quary_nc;         // for reading our memory notecard
integer nc_line;        // what line are we reading
integer iStartValue;

// Stub out OpenSim-only function
vector GetRegionSize() {
    vector rv = <255.0, 255.0, 4096.0>;
    if (!is_SL()) {
        // Comment out this line to run in SecondLife, un-comment it to run in OpenSim
        rv = osGetRegionSize();
        rv.x = (rv.x - 15);
        rv.y = (rv.y - 15);
    }
    return rv;
}

// OpenSim-only function
// Un-comment this to run in SecondLife, comment them to run in OpenSim
SaveNotecard(string s, list l) {
    if (!is_SL()) {
        if (llGetInventoryType("Builders Buddy Memory") != -1) {
            llRemoveInventory("Builders Buddy Memory");
        }
        // Comment out this line to run in SecondLife, un-comment it to run in OpenSim
        osMakeNotecard( "Builders Buddy Memory", record_mem);
    }
}

log(string msg) {
    if (chatty >= 1) {
        llOwnerSay(msg);
    }
}

warning(string msg) {
    if (chatty >= 2) {
        log(msg);
    }
}

debug(string msg) {
    if (chatty >= 3) {
        log(msg);
    }
}

// Hack to detect Second Life vs OpenSim
// Relies on a bug in llParseString2List() in SL
// http://grimore.org/fuss/lsl/bugs#splitting_strings_to_lists
integer is_SL() {
    string sa = "12999";
//    list OS = [1,2,9,9,9];
    list SL = [1,2,999];
    list la = llParseString2List(sa, [], ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]);
    return (la == SL);
}

////////////////////////////////////////////////////////////////////////////////
string first_word(string In_String, string Token)
{
    // This routine searches for the first word in a string,
    // and returns it.  If no word boundary found, returns
    // the whole string.
    if(Token == "") Token = " ";
    integer pos = llSubStringIndex(In_String, Token);

    // Found it?
    if( pos >= 1 )
        return llGetSubString(In_String, 0, pos - 1);
    else
        return In_String;
}

////////////////////////////////////////////////////////////////////////////////
string other_words(string In_String, string Token)
{
    // This routine searches for the other-than-first words in a string,
    // and returns it. If no word boundary found, returns
    // an empty string.
    if( Token == "" ) Token = " ";

    integer pos = llSubStringIndex(In_String, Token);

    // Found it?
    if( pos >= 1 )
        return llGetSubString(In_String, pos + 1, llStringLength(In_String));
    else
        return "";
}

////////////////////////////////////////////////////////////////////////////////
do_move()
{
    integer i = 0;
    vector vLastPos = ZERO_VECTOR;

    // Set rotation
    // (lt3: Do rotation first so moves are into final position)
    llSetRot(rDestRot);

    while( (i < 5) && (llGetPos() != vDestPos) )
    {
        list lParams = [];

        // If we're not there....
        if( llGetPos() != vDestPos )
        {
            // We may be stuck on the ground...
            // Did we move at all compared to last loop?
            if( llGetPos() == vLastPos )
            {
                // Yep, stuck...move straight up 10m (attempt to dislodge)
                lParams = [ PRIM_POSITION, llGetPos() + <0, 0, 10.0> ];
                //llSetPos(llGetPos() + <0, 0, 10.0>);
            } else {
                // Record our spot for 'stuck' detection
                vLastPos = llGetPos();
            }
        }

        // Try to move to destination
        // Upgraded to attempt to use the llSetPrimitiveParams fast-move hack
        // (Newfie, June 2006)
        integer iHops = llAbs(llCeil(llVecDist(llGetPos(), vDestPos) / 10.0));
        integer x;
        for( x = 0; x < iHops; x++ ) {
            lParams += [ PRIM_POSITION, vDestPos ];
        }
        llSetPrimitiveParams(lParams);
        //llSleep(0.1);
        i++;
    }
}

start_move(string sText, key kID)
{
    // Don't move if we've not yet recorded a position
    if( !bRecorded ) return;

    // Also ignore commands from bases with a different owner than us
    //( Anti-hacking measure)
    if( llGetOwner() != llGetOwnerKey(kID) ) return;


    // Calculate our destination position relative to base?
    if(!bAbsolute) {
        // Relative position
        // Calculate our destination position
        sText = other_words(sText, " ");
        list lParams = llParseString2List(sText, [ "|" ], []);
        vector vBase = (vector)llList2String(lParams, 0);
        rotation rBase = (rotation)llList2String(lParams, 1);

        vDestPos = (vOffset * rBase) + vBase;
        rDestRot = rRotation * rBase;
    } else {
        // Sim position
        vDestPos = vOffset;
        rDestRot = rRotation;
    }

    vLimit = GetRegionSize();

    // Make sure our calculated position is within the sim
    if(vDestPos.x < 0.0) vDestPos.x = 0.0;
    if(vDestPos.x > vLimit.x) vDestPos.x = vLimit.x;
    if(vDestPos.y < 0.0) vDestPos.y = 0.0;
    if(vDestPos.y > vLimit.y) vDestPos.y = vLimit.y;
    if(vDestPos.z > 4096.0) vDestPos.z = 4096.0;

    // Turn on our timer to perform the move?
    if( !bNeedMove )
    {
        llSetTimerEvent(fTimerInterval);
        bNeedMove = TRUE;
    }
    return;
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
default
{
    //////////////////////////////////////////////////////////////////////////////////////////
    state_entry()
    {
        // Set up memory constraints
        llSetMemoryLimit(MEM_LIMIT);

        // Open up the listener
        llListen(PRIMCHAN, "", NULL_KEY, "");
        llRegionSay(PRIMCHAN, "READYTOPOS");

//        llOwnerSay("Free memory " + (string)llGetFreeMemory() + "  Limit: " + (string)MEM_LIMIT);
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    on_rez(integer iStart)
    {
        iStartValue = iStart;
        if(llGetInventoryType("Builders Buddy Memory") != -1)
        {
            nc_line = 0;
            g_quary_nc = llGetNotecardLine("Builders Buddy Memory", nc_line);
        }
        else
        {
            // Set the channel to what's specified
            if( iStart != 0 )
            {
                PRIMCHAN = iStart;
                state reset_listeners;
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    listen(integer iChan, string sName, key kID, string sText)
    {
        string sCmd = llToUpper(first_word(sText, " "));

        if( sCmd == "RECORD" )
        {
            // Record position relative to base prim
            sText = other_words(sText, " ");
            list lParams = llParseString2List(sText, [ "|" ], []);
            vector vBase = (vector)llList2String(lParams, 0);
            rotation rBase = (rotation)llList2String(lParams, 1);

            vOffset = (llGetPos() - vBase) / rBase;
            rRotation = llGetRot() / rBase;
            bAbsolute = FALSE;
            bRecorded = TRUE;

            record_mem = [];
            record_mem += "vOffset|" + (string)vOffset;
            record_mem += "rRotation|" + (string)rRotation;
            record_mem += "bAbsolute|" +(string)bAbsolute;
            record_mem += "bRecorded|" +(string)bRecorded ;

            SaveNotecard( "Builders Buddy Memory", record_mem);

            llOwnerSay("Recorded position.");
            return;
        }

        if( sCmd == "RECORDABS" )
        {
            // Record absolute position
            rRotation = llGetRot();
            vOffset = llGetPos();
            bAbsolute = TRUE;
            bRecorded = TRUE;

            record_mem = [];
            record_mem += "vOffset|" +(string)vOffset;
            record_mem += "rRotation|" +(string)rRotation;
            record_mem += "bAbsolute|" +(string)bAbsolute;
            record_mem += "bRecorded|" +(string)bRecorded;

            SaveNotecard( "Builders Buddy Memory", record_mem );

            llOwnerSay("Recorded sim position.");
            return;
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        if( sCmd == "MOVE" )
        {
            start_move(sText, kID);
            return;
        }

        if( sCmd == "MOVESINGLE" )
        {
            // If we haven't gotten this before, position ourselves
            if(!bMovingSingle) {
                // Record that we are a single-prim move
                bMovingSingle = TRUE;

                // Now move it
                start_move(sText, kID);
                return;
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        if( sCmd == "DONE" )
        {
            // We are done, remove script
            if(!is_SL() && llGetInventoryType("Builders Buddy Memory") != -1)
            {
                llRemoveInventory("Builders Buddy Memory");
            }
            llRemoveInventory(llGetScriptName());
            return;
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        if( sCmd == "CLEAN" )
        {
            // Clean up
            llDie();
            return;
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        if( sCmd == "RESET" )
        {
            llResetScript();
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        if (sCmd == "QUIET") {
            chatty = 0;
            return;
        }
        else if (sCmd == "LOG") {
            chatty = 1;
            return;
        }
        else if (sCmd == "WARNING") {
            chatty = 2;
            return;
        }
        else if (sCmd == "DEBUG") {
            chatty = 3;
            return;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    dataserver(key queryid, string data)
    {
        if (!is_SL() && queryid == g_quary_nc)
        {
            if (data != EOF)
            {
                list n = llParseString2List(data, ["|"], []);

                if(llList2String(n, 0) == "vOffset")
                {
                    vOffset = llList2Vector(n, 1);
                }
                else if(llList2String(n, 0) == "rRotation")
                {
                    rRotation = llList2Rot(n, 1);
                }
                else if(llList2String(n, 0) == "bAbsolute")
                {
                    bAbsolute = (integer)llList2String(n, 1);
                }
                else if(llList2String(n, 0) == "bRecorded")
                {
                    bRecorded = (integer)llList2String(n, 1);
                }
                nc_line++;
                g_quary_nc = llGetNotecardLine("Builders Buddy Memory", nc_line);
            }
            else
            {
                //Set the channel to what's specified
                if( iStartValue != 0 )
                {
                    PRIMCHAN = iStartValue;
                    state reset_listeners;
                }
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    timer()
    {
        // Turn ourselves off
        llSetTimerEvent(0.0);

        // Do we need to move?
        if( bNeedMove )
        {
            // Perform the move and clean up
            do_move();

            // If single-prim move, announce to base we're done
            if(bMovingSingle) {
                llRegionSay(PRIMCHAN, "ATDEST");
            }

            // Done moving
            bNeedMove = FALSE;
        }
        return;
    }
}


//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
state reset_listeners
{
    //////////////////////////////////////////////////////////////////////////////////////////
    state_entry()
    {
        state default;
    }
}
