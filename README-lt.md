# Builder's Buddy LT Updates

Layne Thomas has made some additions to the Builder's Buddy scripts found
in the [Second Life Wiki](http://wiki.secondlife.com/wiki/Builders_Buddy),
including the following:

* Support for remote chat commands
* Support for an offset for build position
* Support for a delay in positioning after objects are rezzed
* A patch from Outworldz for [OpenSim use](https://www.outworldz.com/cgi/freescripts.plx?ID=1119)

## Remote Commands

* enable by setting `enableRemote = TRUE`
* set channel with `REMOTE_CHANNEL`

### Commands

* Build
* Clean
* Position

## Build Offset (lt3)

Sometimes the position of the rez box (base) is such that objects are
temporarily visible while being rezzed before being moved into position.
The `buildOffset` setting allows the rez position to be moved some short
distance away from the rez box so the pieces are not visible and will then
be moved into their final position all at once.

This also changes the order of position and rotation in the Component script
so rotations are done first.  This makes the objects appear in their final
location all at once and in their final orientation.

## Position Reference (lt4)

Respond to remote chat command `getpos` with the position and rotation of
the rez box base.

## Delay Position (lt5)

Set `delayPosition = TRUE` to skip positioning the objects when `bulkBuild`
is `TRUE`.  A subsequent `position` command is required to do the final
positioning.

## OpenSim Support (lt6)

The patch from Outworldz:
* Detects between Second Life and OpenSim
* Adds a 1 second delay in rez_object()
* Increases `iRezWait` time by 2 seconds`
* Handle a positioning issue with detecting sim boundaries
* Save recorded positions to a notecard

The scripts in the repository are shipped with two lines that will only
compile on OpenSim uncommented, they must be commented out for the script
to compile in Second Life.

## Memory Limits (lt7)

Set `MEM_LIMIT` to the max memory the script has available.  Both scripts
default to `48000` so there is some savings especially when a large number
of objects are being managed.
